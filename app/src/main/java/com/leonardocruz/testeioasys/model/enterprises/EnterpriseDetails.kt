package com.leonardocruz.testeioasys.model.enterprises

data class EnterpriseDetails(
    val enterprise: Enterprise?,
    val success: Boolean?
) {
    data class Enterprise(
        val city: String?,
        val country: String?,
        val description: String?,
        val email_enterprise: Any?,
        val enterprise_name: String?,
        val enterprise_type: EnterpriseType?,
        val facebook: Any?,
        val id: Int?,
        val linkedin: Any?,
        val own_enterprise: Boolean?,
        val own_shares: Int?,
        val phone: Any?,
        val photo: String?,
        val share_price: Double?,
        val shares: Int?,
        val twitter: Any?,
        val value: Int?
    ) {
        data class EnterpriseType(
            val enterprise_type_name: String?,
            val id: Int?
        )
    }
}