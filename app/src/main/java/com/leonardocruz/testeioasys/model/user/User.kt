package com.leonardocruz.testeioasys.model.user

data class User(
    val email: String,
    val password: String
)