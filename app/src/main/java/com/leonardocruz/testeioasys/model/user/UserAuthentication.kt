package com.leonardocruz.testeioasys.model.user

import java.io.Serializable

data class UserAuthentication (
    val token : String,
    val client : String,
    val uid : String
) : Serializable