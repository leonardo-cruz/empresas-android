package com.leonardocruz.testeioasys.model.user

data class SignResponse(
    val enterprise: Any?,
    val investor: Investor?,
    val success: Boolean?,
    val accessToken : String?
) {
    data class Investor(
        val balance: Double?,
        val city: String?,
        val country: String?,
        val email: String?,
        val first_access: Boolean?,
        val id: Int?,
        val investor_name: String?,
        val photo: Any?,
        val portfolio: Portfolio?,
        val portfolio_value: Double?,
        val super_angel: Boolean?
    ) {
        data class Portfolio(
            val enterprises: List<Any?>?,
            val enterprises_number: Int?
        )
    }
}
