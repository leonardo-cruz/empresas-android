package com.leonardocruz.testeioasys.repository

import com.leonardocruz.testeioasys.model.user.SignResponse
import com.leonardocruz.testeioasys.model.user.User
import com.leonardocruz.testeioasys.network.AuthenticationApi
import com.leonardocruz.testeioasys.network.IoasysClient
import com.leonardocruz.testeioasys.util.Constants.BASE_URL
import retrofit2.Response

class AuthenticationRepository {

    suspend fun signUser(user : User) : Response<SignResponse>{
        return IoasysClient.getInstance<AuthenticationApi>(BASE_URL).signUser(user)
    }

    suspend fun getEnterprise(token : String, client : String, uid : String, id : String) : Response<Any>{
        return IoasysClient.getInstance<AuthenticationApi>(BASE_URL).getEnterprise(token, client, uid, id)
    }
}