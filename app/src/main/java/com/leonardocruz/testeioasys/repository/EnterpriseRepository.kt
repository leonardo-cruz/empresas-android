package com.leonardocruz.testeioasys.repository

import com.leonardocruz.testeioasys.model.enterprises.EnterpriseDetails
import com.leonardocruz.testeioasys.model.enterprises.Enterprises
import com.leonardocruz.testeioasys.network.EnterpriseApi
import com.leonardocruz.testeioasys.network.IoasysClient
import com.leonardocruz.testeioasys.util.Constants
import retrofit2.Response

class EnterpriseRepository {

    suspend fun getEnterprise(token : String, client : String, uid : String, id : String) : Response<EnterpriseDetails> {
        return IoasysClient.getInstance<EnterpriseApi>(Constants.BASE_URL).getEnterprise(token, client, uid, id)
    }

    suspend fun getSearchEnterprise(token : String, client : String, uid : String, name : String) : Response<Enterprises> {
        return IoasysClient.getInstance<EnterpriseApi>(Constants.BASE_URL).getSearchEnterprises(token, client, uid, name)
    }
}