package com.leonardocruz.testeioasys.di

import com.leonardocruz.testeioasys.ui.login.viewmodel.AuthenticationViewModel
import com.leonardocruz.testeioasys.ui.home.viewmodel.EnterpriseViewModel
import com.leonardocruz.testeioasys.repository.AuthenticationRepository
import com.leonardocruz.testeioasys.repository.EnterpriseRepository
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { (repository : AuthenticationRepository) -> AuthenticationViewModel(repository) }
    viewModel { (repository : EnterpriseRepository) -> EnterpriseViewModel(repository) }
}