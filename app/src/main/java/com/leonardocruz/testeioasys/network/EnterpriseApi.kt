package com.leonardocruz.testeioasys.network

import com.leonardocruz.testeioasys.model.enterprises.EnterpriseDetails
import com.leonardocruz.testeioasys.model.enterprises.Enterprises
import com.leonardocruz.testeioasys.util.Constants.ACCESS_TOKEN
import com.leonardocruz.testeioasys.util.Constants.CLIENT
import com.leonardocruz.testeioasys.util.Constants.ID
import com.leonardocruz.testeioasys.util.Constants.NAME
import com.leonardocruz.testeioasys.util.Constants.UID
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface EnterpriseApi {

    @GET("enterprises/{id}")
    suspend fun getEnterprise(
        @Header(ACCESS_TOKEN) token : String,
        @Header(CLIENT) client : String,
        @Header(UID) uid : String,
        @Path(ID) id : String
    ) : Response<EnterpriseDetails>

    @GET("enterprises")
    suspend fun getSearchEnterprises(
        @Header(ACCESS_TOKEN) token : String,
        @Header(CLIENT) client : String,
        @Header(UID) uid : String,
        @Query(NAME) name : String
    ) : Response<Enterprises>
}