package com.leonardocruz.testeioasys.network

import com.leonardocruz.testeioasys.model.user.SignResponse
import com.leonardocruz.testeioasys.model.user.User
import com.leonardocruz.testeioasys.util.Constants.ACCESS_TOKEN
import com.leonardocruz.testeioasys.util.Constants.CLIENT
import com.leonardocruz.testeioasys.util.Constants.ID
import com.leonardocruz.testeioasys.util.Constants.UID
import retrofit2.Response
import retrofit2.http.*

interface AuthenticationApi {

    @POST("users/auth/sign_in")
    suspend fun signUser(
        @Body body : User
    ) : Response<SignResponse>

    @GET("enterprises/{id}")
    suspend fun getEnterprise(
        @Header(ACCESS_TOKEN) token : String,
        @Header(CLIENT) client : String,
        @Header(UID) uid : String,
        @Path(ID) id : String
    ) : Response<Any>

}