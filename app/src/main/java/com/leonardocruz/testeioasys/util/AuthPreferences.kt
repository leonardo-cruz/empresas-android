package com.leonardocruz.testeioasys.util

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.leonardocruz.testeioasys.model.user.UserAuthentication

object AuthPreferences {

    fun readAuth(context: Context): UserAuthentication {
        val prefs = context.getSharedPreferences(
            Constants.AUTH,
            AppCompatActivity.MODE_PRIVATE
        )
        val json = prefs.getString(Constants.AUTH, null)
        val auth = Gson().fromJson(json, UserAuthentication::class.java)
        return auth
    }

    fun savePreferences(context: Context, auth : UserAuthentication) {
        val prefs = context.getSharedPreferences(
            Constants.AUTH,
            AppCompatActivity.MODE_PRIVATE
        )
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(auth)
        editor.putString(Constants.AUTH, json).apply()
    }
}