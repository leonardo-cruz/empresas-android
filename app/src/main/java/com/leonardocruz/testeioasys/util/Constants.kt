package com.leonardocruz.testeioasys.util

object Constants {
    const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    const val ACCESS_TOKEN = "access-token"
    const val CLIENT = "client"
    const val UID = "uid"
    const val NAME = "name"
    const val URL = "https://empresas.ioasys.com.br/"
    const val ID = "id"
    const val AUTH = "auth"
}