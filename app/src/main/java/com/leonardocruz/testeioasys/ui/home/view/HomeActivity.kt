package com.leonardocruz.testeioasys.ui.home.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.leonardocruz.testeioasys.ui.home.viewmodel.EnterpriseViewModel
import com.leonardocruz.testeioasys.R
import com.leonardocruz.testeioasys.model.user.UserAuthentication
import com.leonardocruz.testeioasys.repository.EnterpriseRepository
import com.leonardocruz.testeioasys.util.AuthPreferences
import com.leonardocruz.testeioasys.ui.home.fragment.SearchFragment
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class HomeActivity : AppCompatActivity() {
    private val mViewModel: EnterpriseViewModel by viewModel {
        parametersOf(EnterpriseRepository())
    }

    private lateinit var auth: UserAuthentication
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        replaceFragments(SearchFragment(), this)
        auth = AuthPreferences.readAuth(this)
    }

    private fun replaceFragments(fragment: Fragment, context: Context) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container_fragment_search, fragment)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        val search = menu?.findItem(R.id.nav_search)
        val searchView = search?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                if (!p0.isNullOrEmpty()) {
                    mViewModel.getSearchEnterprise(auth.token, auth.client, auth.uid, p0)
                }
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

        })

        return super.onCreateOptionsMenu(menu)
    }

    companion object {
        fun launchActivity(
            context: Context,
        ) {
            val intent = Intent(context, HomeActivity::class.java)
            context.startActivity(intent)
        }
    }
}
