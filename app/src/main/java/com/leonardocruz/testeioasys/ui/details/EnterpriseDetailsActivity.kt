package com.leonardocruz.testeioasys.ui.details

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.leonardocruz.testeioasys.ui.home.viewmodel.EnterpriseViewModel
import com.leonardocruz.testeioasys.databinding.ActivityEnterpriseDetailsBinding
import com.leonardocruz.testeioasys.model.user.UserAuthentication
import com.leonardocruz.testeioasys.repository.EnterpriseRepository
import com.leonardocruz.testeioasys.util.Constants
import com.leonardocruz.testeioasys.util.Constants.ID
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class EnterpriseDetailsActivity : AppCompatActivity() {
    private lateinit var binding : ActivityEnterpriseDetailsBinding
    private val mViewModel : EnterpriseViewModel by viewModel{
        parametersOf(EnterpriseRepository())
    }
    private lateinit var auth : UserAuthentication
    lateinit var id : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterpriseDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        id = intent.getStringExtra(ID).toString()
        auth = intent.getSerializableExtra(Constants.AUTH) as UserAuthentication
        initObservers()
        binding.detailsBackButton.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initObservers() {
        mViewModel.getEnterpriseShow(auth.token, auth.client, auth.uid, id)
        mViewModel.enterpriseDetailsData.observe(this, {
            binding.tvDescriptionEnterprise.text = it.description
            Glide.with(this).load(Constants.URL + it.photo).into(binding.ivDetailsEnterprise)
            binding.tvNameEnterprise.text = it.enterprise_name
        })
    }


    companion object {
        fun launchActivity(
            context: Context,
            auth: UserAuthentication? = null,
            id : String
        ) {
            val intent = Intent(context, EnterpriseDetailsActivity::class.java)
            intent.putExtra(Constants.AUTH, auth)
            intent.putExtra(ID, id)
            context.startActivity(intent)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}