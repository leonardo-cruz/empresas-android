package com.leonardocruz.testeioasys.ui.login.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.leonardocruz.testeioasys.model.user.User
import com.leonardocruz.testeioasys.model.user.UserAuthentication
import com.leonardocruz.testeioasys.repository.AuthenticationRepository
import com.leonardocruz.testeioasys.util.Constants.ACCESS_TOKEN
import com.leonardocruz.testeioasys.util.Constants.CLIENT
import com.leonardocruz.testeioasys.util.Constants.UID
import kotlinx.coroutines.*

class AuthenticationViewModel(val repository: AuthenticationRepository) : ViewModel() {
    private var job: Job? = null
    val mutableAuth: MutableLiveData<UserAuthentication> = MutableLiveData()
    val authData = mutableAuth
    val mutableError: MutableLiveData<Boolean> = MutableLiveData()
    val errorData = mutableError
    val mutableErrorAuth: MutableLiveData<Boolean> = MutableLiveData()
    val errorAuthData = mutableErrorAuth
    val progressBar: MutableLiveData<Boolean> = MutableLiveData()

    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        onError(throwable.localizedMessage)
    }

    fun signIn(user: User) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            progressBar.postValue(true)
            val response = repository.signUser(user)
            withContext(Dispatchers.Main) {
                progressBar.value = false
                if (response.isSuccessful) {
                    val header = response.headers()
                    val token = header.values(ACCESS_TOKEN)
                    val client = header.values(CLIENT)
                    val uid = header.values(UID)
                    val userAuth = UserAuthentication(token[0], client[0], uid[0])
                    mutableAuth.value = userAuth
                } else {
                    mutableErrorAuth.value = true
                }
            }
        }
    }

    private fun onError(localizedMessage: String?) {
        Log.d("AUTH", localizedMessage.toString())
        mutableError.postValue(true)
    }
}