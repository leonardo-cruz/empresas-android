package com.leonardocruz.testeioasys.ui.home.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.leonardocruz.testeioasys.model.enterprises.EnterpriseDetails
import com.leonardocruz.testeioasys.model.enterprises.Enterprises
import com.leonardocruz.testeioasys.repository.EnterpriseRepository
import kotlinx.coroutines.*

class EnterpriseViewModel(val repository: EnterpriseRepository) : ViewModel() {
    private var job: Job? = null
    private val mutableEnterpriseDetails: MutableLiveData<EnterpriseDetails.Enterprise> = MutableLiveData()
    val enterpriseDetailsData = mutableEnterpriseDetails
    private val mutableEnterpriseSearch: MutableLiveData<Enterprises> = MutableLiveData()
    val enterpriseSearchData = mutableEnterpriseSearch
    val mutableError: MutableLiveData<String> = MutableLiveData()
    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        onError(throwable.localizedMessage)
    }

    fun getEnterpriseShow(token: String, client: String, uid: String, id : String) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = repository.getEnterprise(token, client, uid, id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val header = response.body()
                    mutableEnterpriseDetails.value = header?.enterprise
                } else {
                    Log.i("ENTERPRISE", response.message())
                    mutableError.value = response.message()
                }
            }
        }
    }

    fun getSearchEnterprise(token: String, client: String, uid: String, name: String) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = repository.getSearchEnterprise(token, client, uid, name)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val enterprises = response.body()
                        mutableEnterpriseSearch.value = enterprises
                } else {
                    Log.i("ENTERPRISE", response.message())
                    mutableError.value = response.message()                }
            }
        }
    }

    private fun onError(message: String?) {
        Log.i("ENTERPRISE", message.toString())
        mutableError.postValue(message)
    }
}