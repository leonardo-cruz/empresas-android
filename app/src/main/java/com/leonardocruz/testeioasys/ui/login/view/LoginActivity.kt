package com.leonardocruz.testeioasys.ui.login.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.leonardocruz.testeioasys.ui.home.view.HomeActivity
import com.leonardocruz.testeioasys.R
import com.leonardocruz.testeioasys.databinding.ActivityLoginBinding
import com.leonardocruz.testeioasys.model.user.User
import com.leonardocruz.testeioasys.repository.AuthenticationRepository
import com.leonardocruz.testeioasys.util.AuthPreferences
import com.leonardocruz.testeioasys.ui.login.viewmodel.AuthenticationViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class LoginActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLoginBinding
    private val mViewModel : AuthenticationViewModel by viewModel{
        parametersOf(AuthenticationRepository())
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        initObservers()

    }

    private fun initObservers() {
        mViewModel.authData.observe(this, {
            AuthPreferences.savePreferences(this, it)
            HomeActivity.launchActivity(this)
        })
        mViewModel.errorData.observe(this, {
            Toast.makeText(this, getString(R.string.generic_error), Toast.LENGTH_SHORT).show()
            binding.inputPassword.isEnabled = true
            binding.inputEmail.isEnabled = true
        })
        mViewModel.errorAuthData.observe(this, {
            binding.tvErrorLogin.visibility = VISIBLE
            binding.inputPassword.isEnabled = true
            binding.inputEmail.isEnabled = true
        })
        mViewModel.progressBar.observe(this, {
            if(it) binding.progressBarLogin.visibility = VISIBLE
            else binding.progressBarLogin.visibility = GONE
        })
    }

    private fun initView() {
        binding.btLogin.setOnClickListener {
            val email = binding.inputEmail.text.toString()
            val password = binding.inputPassword.text.toString()
            if(email.isNullOrEmpty()){
                input_email.requestFocus()
                Toast.makeText(this, getString(R.string.empty_email), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(password.isNullOrEmpty()){
                input_password.requestFocus()
                Toast.makeText(this, getString(R.string.empty_password), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            binding.tvErrorLogin.visibility = GONE
            mViewModel.signIn(User(email, password))
            binding.inputPassword.text?.clear()
            binding.inputPassword.isEnabled = false
            binding.inputEmail.isEnabled = false
        }
    }
}