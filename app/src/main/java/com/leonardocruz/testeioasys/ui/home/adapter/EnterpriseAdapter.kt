package com.leonardocruz.testeioasys.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.leonardocruz.testeioasys.R
import com.leonardocruz.testeioasys.model.enterprises.Enterprises
import com.leonardocruz.testeioasys.util.Constants.URL
import kotlinx.android.synthetic.main.item_enterprise.view.*

class EnterpriseAdapter(var listEnterprise: MutableList<Enterprises.Enterprise>, val itemListener : (Enterprises.Enterprise) -> Unit) :
    RecyclerView.Adapter<EnterpriseAdapter.MyViewHolder>() {
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun onBind(enterprise : Enterprises.Enterprise){
            Glide.with(itemView).load(URL + enterprise.photo).into(itemView.iv_enterprise_item)
            itemView.tv_title_enterprise_item.text = enterprise.enterprise_name
            itemView.tv_country_enterprise_item.text = enterprise.country
            itemView.tv_negocio_enterprise_item.text = enterprise.enterprise_type?.enterprise_type_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_enterprise, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind(listEnterprise[position])
        holder.itemView.setOnClickListener {
            itemListener(listEnterprise[position])
        }
    }

    override fun getItemCount(): Int {
        return listEnterprise.size
    }

    fun updateList(newList: MutableList<Enterprises.Enterprise>){
        listEnterprise = newList
        notifyDataSetChanged()
    }
}