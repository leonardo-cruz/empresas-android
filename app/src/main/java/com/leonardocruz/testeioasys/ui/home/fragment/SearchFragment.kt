package com.leonardocruz.testeioasys.ui.home.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.leonardocruz.testeioasys.ui.home.adapter.EnterpriseAdapter
import com.leonardocruz.testeioasys.ui.details.EnterpriseDetailsActivity
import com.leonardocruz.testeioasys.ui.home.viewmodel.EnterpriseViewModel
import com.leonardocruz.testeioasys.R
import com.leonardocruz.testeioasys.model.user.UserAuthentication
import com.leonardocruz.testeioasys.model.enterprises.Enterprises
import com.leonardocruz.testeioasys.repository.EnterpriseRepository
import com.leonardocruz.testeioasys.util.AuthPreferences
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.core.parameter.parametersOf

class SearchFragment : Fragment() {
    private val mViewModel: EnterpriseViewModel by sharedViewModel {
        parametersOf(EnterpriseRepository())
    }
    private lateinit var mAdapter: EnterpriseAdapter
    private lateinit var auth: UserAuthentication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = AuthPreferences.readAuth(requireContext())
        initViews()
        initObservers()
    }

    private fun initObservers() {
        mViewModel.enterpriseSearchData.observe(viewLifecycleOwner, {
            tv_click_search.visibility = GONE
            if (it.enterprisesList!!.isEmpty()) view?.tv_empty_search?.visibility = VISIBLE
            else view?.tv_empty_search?.visibility = GONE
            mAdapter.updateList(it.enterprisesList as MutableList<Enterprises.Enterprise>)
        })
        mViewModel.mutableError.observe(viewLifecycleOwner, {
            Toast.makeText(requireContext(), getString(R.string.generic_error), Toast.LENGTH_SHORT)
        })
    }

    private fun initViews() {
        mAdapter = EnterpriseAdapter(mutableListOf()) { enterprise -> clickEnterprise(enterprise) }
        rv_enterprises.adapter = mAdapter
        rv_enterprises.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun clickEnterprise(enterprise: Enterprises.Enterprise) {
        EnterpriseDetailsActivity.launchActivity(requireContext(), auth, enterprise.id.toString())
    }
}